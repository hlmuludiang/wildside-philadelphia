//
//  WildSideModel.swift
//  WildSide Philadelphia
//
//  Created by Versatile Systems, Inc on 4/10/15.
//  Copyright (c) 2015 Muludiang IT Services. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import CoreData

class WildSideModel: NSObject {
  
  // Source: https://www.opendataphilly.org/dataset/philadelphia-police-part-one-crime-incidents
  // Reference: http://phlapi.com/crimeapi.html
  
  let crimeJsonData = "{\"displayFieldName\":\"DC_DIST\",\"fieldAliases\":{\"DC_DIST\":\"DC_DIST\",\"SECTOR\":\"SECTOR\",\"DISPATCH_DATE_TIME\":\"DISPATCH_DATE_TIME\",\"DISPATCH_DATE\":\"DISPATCH_DATE\",\"DISPATCH_TIME\":\"DISPATCH_TIME\",\"DC_KEY\":\"DC_KEY\",\"LOCATION_BLOCK\":\"LOCATION_BLOCK\",\"UCR_GENERAL\":\"UCR_GENERAL\",\"OBJECTID\":\"OBJECTID\",\"TEXT_GENERAL_CODE\":\"TEXT_GENERAL_CODE\",\"POINT_X\":\"POINT_X\",\"POINT_Y\":\"POINT_Y\",\"HOUR\":\"HOUR\",\"ORIG_FID\":\"ORIG_FID\"},\"geometryType\":\"esriGeometryPoint\",\"spatialReference\":{\"wkid\":4269,\"latestWkid\":4269},\"fields\":[{\"name\":\"DC_DIST\",\"type\":\"esriFieldTypeString\",\"alias\":\"DC_DIST\",\"length\":2},{\"name\":\"SECTOR\",\"type\":\"esriFieldTypeString\",\"alias\":\"SECTOR\",\"length\":1},{\"name\":\"DISPATCH_DATE_TIME\",\"type\":\"esriFieldTypeDate\",\"alias\":\"DISPATCH_DATE_TIME\",\"length\":8},{\"name\":\"DISPATCH_DATE\",\"type\":\"esriFieldTypeString\",\"alias\":\"DISPATCH_DATE\",\"length\":30},{\"name\":\"DISPATCH_TIME\",\"type\":\"esriFieldTypeString\",\"alias\":\"DISPATCH_TIME\",\"length\":30},{\"name\":\"DC_KEY\",\"type\":\"esriFieldTypeString\",\"alias\":\"DC_KEY\",\"length\":12},{\"name\":\"LOCATION_BLOCK\",\"type\":\"esriFieldTypeString\",\"alias\":\"LOCATION_BLOCK\",\"length\":45},{\"name\":\"UCR_GENERAL\",\"type\":\"esriFieldTypeString\",\"alias\":\"UCR_GENERAL\",\"length\":50},{\"name\":\"OBJECTID\",\"type\":\"esriFieldTypeOID\",\"alias\":\"OBJECTID\"},{\"name\":\"TEXT_GENERAL_CODE\",\"type\":\"esriFieldTypeString\",\"alias\":\"TEXT_GENERAL_CODE\",\"length\":255},{\"name\":\"POINT_X\",\"type\":\"esriFieldTypeDouble\",\"alias\":\"POINT_X\"},{\"name\":\"POINT_Y\",\"type\":\"esriFieldTypeDouble\",\"alias\":\"POINT_Y\"},{\"name\":\"HOUR\",\"type\":\"esriFieldTypeSmallInteger\",\"alias\":\"HOUR\"},{\"name\":\"ORIG_FID\",\"type\":\"esriFieldTypeInteger\",\"alias\":\"ORIG_FID\"}],\"features\":[{\"attributes\":{\"DC_DIST\":\"26\",\"SECTOR\":\"3\",\"DISPATCH_DATE_TIME\":1426508700000,\"DISPATCH_DATE\":\"2015-03-16\",\"DISPATCH_TIME\":\"12:25:00\",\"DC_KEY\":\"201526012926\",\"LOCATION_BLOCK\":\"1800 BLOCK E LEHIGH AV\",\"UCR_GENERAL\":\"100\",\"OBJECTID\":462,\"TEXT_GENERAL_CODE\":\"Homicide - Gross Negligence\",\"POINT_X\":-75.125500169999995,\"POINT_Y\":39.989780430000003,\"HOUR\":null,\"ORIG_FID\":89425548},\"geometry\":{\"x\":-75.125500166999984,\"y\":39.989780434000068}},{\"attributes\":{\"DC_DIST\":\"12\",\"SECTOR\":\"4\",\"DISPATCH_DATE_TIME\":1426262100000,\"DISPATCH_DATE\":\"2015-03-13\",\"DISPATCH_TIME\":\"15:55:00\",\"DC_KEY\":\"201512018907\",\"LOCATION_BLOCK\":\"1700 BLOCK S 58TH ST\",\"UCR_GENERAL\":\"100\",\"OBJECTID\":474,\"TEXT_GENERAL_CODE\":\"Homicide - Criminal \",\"POINT_X\":-75.230912099999998,\"POINT_Y\":39.937818640000003,\"HOUR\":null,\"ORIG_FID\":89428849},\"geometry\":{\"x\":-75.230912101999934,\"y\":39.937818637000078}},{\"attributes\":{\"DC_DIST\":\"18\",\"SECTOR\":\"1\",\"DISPATCH_DATE_TIME\":1426542780000,\"DISPATCH_DATE\":\"2015-03-16\",\"DISPATCH_TIME\":\"21:53:00\",\"DC_KEY\":\"201518016092\",\"LOCATION_BLOCK\":\"6000 BLOCK IRVING ST\",\"UCR_GENERAL\":\"100\",\"OBJECTID\":632,\"TEXT_GENERAL_CODE\":\"Homicide - Criminal\",\"POINT_X\":-75.242771509999997,\"POINT_Y\":39.957385410000001,\"HOUR\":null,\"ORIG_FID\":89565210},\"geometry\":{\"x\":-75.242771510999944,\"y\":39.957385407000061}},{\"attributes\":{\"DC_DIST\":\"24\",\"SECTOR\":\"2\",\"DISPATCH_DATE_TIME\":1427367720000,\"DISPATCH_DATE\":\"2015-03-26\",\"DISPATCH_TIME\":\"11:02:00\",\"DC_KEY\":\"201524025206\",\"LOCATION_BLOCK\":\"2000 BLOCK E CAMBRIA ST\",\"UCR_GENERAL\":\"100\",\"OBJECTID\":1031,\"TEXT_GENERAL_CODE\":\"Homicide - Criminal\",\"POINT_X\":-75.116726900000003,\"POINT_Y\":39.988706350000001,\"HOUR\":null,\"ORIG_FID\":89391611},\"geometry\":{\"x\":-75.116726898999957,\"y\":39.988706346000072}},{\"attributes\":{\"DC_DIST\":\"18\",\"SECTOR\":\"2\",\"DISPATCH_DATE_TIME\":1426833720000,\"DISPATCH_DATE\":\"2015-03-20\",\"DISPATCH_TIME\":\"06:42:00\",\"DC_KEY\":\"201518016829\",\"LOCATION_BLOCK\":\"5100 BLOCK SPRUCE ST\",\"UCR_GENERAL\":\"100\",\"OBJECTID\":1032,\"TEXT_GENERAL_CODE\":\"Homicide - Criminal\",\"POINT_X\":-75.224080180000001,\"POINT_Y\":39.954400870000001,\"HOUR\":null,\"ORIG_FID\":89391612},\"geometry\":{\"x\":-75.224080181999966,\"y\":39.95440086800005}},{\"attributes\":{\"DC_DIST\":\"25\",\"SECTOR\":\"4\",\"DISPATCH_DATE_TIME\":1425920340000,\"DISPATCH_DATE\":\"2015-03-09\",\"DISPATCH_TIME\":\"16:59:00\",\"DC_KEY\":\"201525018241\",\"LOCATION_BLOCK\":\"2800 BLOCK N 12TH ST\",\"UCR_GENERAL\":\"100\",\"OBJECTID\":1068,\"TEXT_GENERAL_CODE\":\"Homicide - Criminal\",\"POINT_X\":-75.150518390000002,\"POINT_Y\":39.995064790000001,\"HOUR\":null,\"ORIG_FID\":89399677},\"geometry\":{\"x\":-75.15051839399996,\"y\":39.995064786000057}},{\"attributes\":{\"DC_DIST\":\"15\",\"SECTOR\":\"2\",\"DISPATCH_DATE_TIME\":1427971980000,\"DISPATCH_DATE\":\"2015-04-02\",\"DISPATCH_TIME\":\"10:53:00\",\"DC_KEY\":\"201515029452\",\"LOCATION_BLOCK\":\"4300 BLOCK MAGEE AV\",\"UCR_GENERAL\":\"100\",\"OBJECTID\":1138,\"TEXT_GENERAL_CODE\":\"Homicide - Gross Negligence\",\"POINT_X\":-75.052035450000005,\"POINT_Y\":40.027403479999997,\"HOUR\":null,\"ORIG_FID\":89425542},\"geometry\":{\"x\":-75.05203544799997,\"y\":40.027403482000068}},{\"attributes\":{\"DC_DIST\":\"15\",\"SECTOR\":\"1\",\"DISPATCH_DATE_TIME\":1427436600000,\"DISPATCH_DATE\":\"2015-03-27\",\"DISPATCH_TIME\":\"06:10:00\",\"DC_KEY\":\"201515027369\",\"LOCATION_BLOCK\":\"2800 BLOCK REYNOLDS ST\",\"UCR_GENERAL\":\"100\",\"OBJECTID\":1145,\"TEXT_GENERAL_CODE\":\"Homicide - Criminal \",\"POINT_X\":-75.064897759999994,\"POINT_Y\":40.001240879999997,\"HOUR\":null,\"ORIG_FID\":89427248},\"geometry\":{\"x\":-75.064897762999976,\"y\":40.001240876000054}},{\"attributes\":{\"DC_DIST\":\"15\",\"SECTOR\":\"1\",\"DISPATCH_DATE_TIME\":1427500680000,\"DISPATCH_DATE\":\"2015-03-27\",\"DISPATCH_TIME\":\"23:58:00\",\"DC_KEY\":\"201515027688\",\"LOCATION_BLOCK\":\"4300 BLOCK ELIZABETH ST\",\"UCR_GENERAL\":\"100\",\"OBJECTID\":1331,\"TEXT_GENERAL_CODE\":\"Homicide - Criminal\",\"POINT_X\":-75.091436659999999,\"POINT_Y\":40.013797580000002,\"HOUR\":null,\"ORIG_FID\":89364695},\"geometry\":{\"x\":-75.091436661999978,\"y\":40.013797582000052}},{\"attributes\":{\"DC_DIST\":\"16\",\"SECTOR\":\"2\",\"DISPATCH_DATE_TIME\":1427035740000,\"DISPATCH_DATE\":\"2015-03-22\",\"DISPATCH_TIME\":\"14:49:00\",\"DC_KEY\":\"201516011957\",\"LOCATION_BLOCK\":\"600 BLOCK N PRESTON ST\",\"UCR_GENERAL\":\"100\",\"OBJECTID\":2004,\"TEXT_GENERAL_CODE\":\"Homicide - Criminal\",\"POINT_X\":-75.20353412,\"POINT_Y\":39.964193450000003,\"HOUR\":null,\"ORIG_FID\":89370268},\"geometry\":{\"x\":-75.203534121999951,\"y\":39.964193447000071}},{\"attributes\":{\"DC_DIST\":\"07\",\"SECTOR\":\"2\",\"DISPATCH_DATE_TIME\":1427802540000,\"DISPATCH_DATE\":\"2015-03-31\",\"DISPATCH_TIME\":\"11:49:00\",\"DC_KEY\":\"201507008711\",\"LOCATION_BLOCK\":\"9800 BLOCK BUSTLETON AV\",\"UCR_GENERAL\":\"100\",\"OBJECTID\":2246,\"TEXT_GENERAL_CODE\":\"Homicide - Gross Negligence\",\"POINT_X\":-75.032064820000002,\"POINT_Y\":40.092057220000001,\"HOUR\":null,\"ORIG_FID\":89391287},\"geometry\":{\"x\":-75.032064823999974,\"y\":40.092057219000026}},{\"attributes\":{\"DC_DIST\":\"24\",\"SECTOR\":\"2\",\"DISPATCH_DATE_TIME\":1426832580000,\"DISPATCH_DATE\":\"2015-03-20\",\"DISPATCH_TIME\":\"06:23:00\",\"DC_KEY\":\"201524023245\",\"LOCATION_BLOCK\":\"3100 BLOCK HARTVILLE ST\",\"UCR_GENERAL\":\"100\",\"OBJECTID\":2350,\"TEXT_GENERAL_CODE\":\"Homicide - Criminal \",\"POINT_X\":-75.120044190000002,\"POINT_Y\":39.99599663,\"HOUR\":null,\"ORIG_FID\":89428844},\"geometry\":{\"x\":-75.120044186999962,\"y\":39.995996628000057}},{\"attributes\":{\"DC_DIST\":\"35\",\"SECTOR\":\"3\",\"DISPATCH_DATE_TIME\":1427283360000,\"DISPATCH_DATE\":\"2015-03-25\",\"DISPATCH_TIME\":\"11:36:00\",\"DC_KEY\":\"201535023313\",\"LOCATION_BLOCK\":\"5900 BLOCK N 19TH ST\",\"UCR_GENERAL\":\"100\",\"OBJECTID\":3604,\"TEXT_GENERAL_CODE\":\"Homicide - Criminal \",\"POINT_X\":-75.152118920000007,\"POINT_Y\":40.043901689999998,\"HOUR\":null,\"ORIG_FID\":89427243},\"geometry\":{\"x\":-75.152118923999979,\"y\":40.04390169100003}},{\"attributes\":{\"DC_DIST\":\"25\",\"SECTOR\":\"4\",\"DISPATCH_DATE_TIME\":1427565360000,\"DISPATCH_DATE\":\"2015-03-28\",\"DISPATCH_TIME\":\"17:56:00\",\"DC_KEY\":\"201525023503\",\"LOCATION_BLOCK\":\"700 BLOCK W RUSSELL ST\",\"UCR_GENERAL\":\"100\",\"OBJECTID\":4145,\"TEXT_GENERAL_CODE\":\"Homicide - Criminal\",\"POINT_X\":-75.142121360000004,\"POINT_Y\":40.003829570000001,\"HOUR\":null,\"ORIG_FID\":89399698},\"geometry\":{\"x\":-75.142121360999965,\"y\":40.003829572000029}},{\"attributes\":{\"DC_DIST\":\"26\",\"SECTOR\":\"3\",\"DISPATCH_DATE_TIME\":1428101160000,\"DISPATCH_DATE\":\"2015-04-03\",\"DISPATCH_TIME\":\"22:46:00\",\"DC_KEY\":\"201526016200\",\"LOCATION_BLOCK\":\"900 BLOCK N DELAWARE AV\",\"UCR_GENERAL\":\"100\",\"OBJECTID\":4180,\"TEXT_GENERAL_CODE\":\"Homicide - Gross Negligence\",\"POINT_X\":-75.134836120000003,\"POINT_Y\":39.963998119999999,\"HOUR\":null,\"ORIG_FID\":89413084},\"geometry\":{\"x\":-75.134836123999946,\"y\":39.963998115000038}},{\"attributes\":{\"DC_DIST\":\"15\",\"SECTOR\":\"3\",\"DISPATCH_DATE_TIME\":1427116860000,\"DISPATCH_DATE\":\"2015-03-23\",\"DISPATCH_TIME\":\"13:21:00\",\"DC_KEY\":\"201515026053\",\"LOCATION_BLOCK\":\"7200 BLOCK SACKETT ST\",\"UCR_GENERAL\":\"100\",\"OBJECTID\":4213,\"TEXT_GENERAL_CODE\":\"Homicide - Gross Negligence\",\"POINT_X\":-75.049041279999997,\"POINT_Y\":40.038866429999999,\"HOUR\":null,\"ORIG_FID\":89424841},\"geometry\":{\"x\":-75.049041279999983,\"y\":40.038866425000037}},{\"attributes\":{\"DC_DIST\":\"15\",\"SECTOR\":\"1\",\"DISPATCH_DATE_TIME\":1427201220000,\"DISPATCH_DATE\":\"2015-03-24\",\"DISPATCH_TIME\":\"12:47:00\",\"DC_KEY\":\"201515026387\",\"LOCATION_BLOCK\":\"4700 BLOCK FRANKFORD AVE\",\"UCR_GENERAL\":\"100\",\"OBJECTID\":4217,\"TEXT_GENERAL_CODE\":\"Homicide - Criminal \",\"POINT_X\":-75.082910670000004,\"POINT_Y\":40.017691169999999,\"HOUR\":null,\"ORIG_FID\":89427244},\"geometry\":{\"x\":-75.082910673999947,\"y\":40.017691170000035}},{\"attributes\":{\"DC_DIST\":\"19\",\"SECTOR\":\"2\",\"DISPATCH_DATE_TIME\":1426294140000,\"DISPATCH_DATE\":\"2015-03-14\",\"DISPATCH_TIME\":\"00:49:00\",\"DC_KEY\":\"201519022763\",\"LOCATION_BLOCK\":\"5200 BLOCK MARKET ST\",\"UCR_GENERAL\":\"100\",\"OBJECTID\":4250,\"TEXT_GENERAL_CODE\":\"Homicide - Criminal \",\"POINT_X\":-75.226824949999994,\"POINT_Y\":39.960287110000003,\"HOUR\":null,\"ORIG_FID\":89440379},\"geometry\":{\"x\":-75.226824951999959,\"y\":39.960287113000049}},{\"attributes\":{\"DC_DIST\":\"14\",\"SECTOR\":\"2\",\"DISPATCH_DATE_TIME\":1428350580000,\"DISPATCH_DATE\":\"2015-04-06\",\"DISPATCH_TIME\":\"20:03:00\",\"DC_KEY\":\"201514025715\",\"LOCATION_BLOCK\":\"0 BLOCK E ASHMEAD ST\",\"UCR_GENERAL\":\"100\",\"OBJECTID\":4776,\"TEXT_GENERAL_CODE\":\"Homicide - Criminal\",\"POINT_X\":-75.166021670000006,\"POINT_Y\":40.031424729999998,\"HOUR\":null,\"ORIG_FID\":89395972},\"geometry\":{\"x\":-75.166021665999949,\"y\":40.031424733000051}},{\"attributes\":{\"DC_DIST\":\"19\",\"SECTOR\":\"1\",\"DISPATCH_DATE_TIME\":1426192740000,\"DISPATCH_DATE\":\"2015-03-12\",\"DISPATCH_TIME\":\"20:39:00\",\"DC_KEY\":\"201519022387\",\"LOCATION_BLOCK\":\"6400 BLOCK WOODCREST AVE\",\"UCR_GENERAL\":\"100\",\"OBJECTID\":4786,\"TEXT_GENERAL_CODE\":\"Homicide - Criminal\",\"POINT_X\":-75.253374500000007,\"POINT_Y\":39.982109680000001,\"HOUR\":null,\"ORIG_FID\":89399705},\"geometry\":{\"x\":-75.253374496999982,\"y\":39.982109682000043}},{\"attributes\":{\"DC_DIST\":\"19\",\"SECTOR\":\"3\",\"DISPATCH_DATE_TIME\":1427554800000,\"DISPATCH_DATE\":\"2015-03-28\",\"DISPATCH_TIME\":\"15:00:00\",\"DC_KEY\":\"201519027737\",\"LOCATION_BLOCK\":\"4900 BLOCK WYNNEFIELD AVE\",\"UCR_GENERAL\":\"100\",\"OBJECTID\":4867,\"TEXT_GENERAL_CODE\":\"Homicide - Criminal \",\"POINT_X\":-75.223893810000007,\"POINT_Y\":39.991438979999998,\"HOUR\":null,\"ORIG_FID\":89427247},\"geometry\":{\"x\":-75.223893808999946,\"y\":39.99143897600004}},{\"attributes\":{\"DC_DIST\":\"12\",\"SECTOR\":\"4\",\"DISPATCH_DATE_TIME\":1428278340000,\"DISPATCH_DATE\":\"2015-04-05\",\"DISPATCH_TIME\":\"23:59:00\",\"DC_KEY\":\"201512025773\",\"LOCATION_BLOCK\":\"5400 BLOCK WILLOWS AVE\",\"UCR_GENERAL\":\"100\",\"OBJECTID\":4875,\"TEXT_GENERAL_CODE\":\"Homicide - Criminal \",\"POINT_X\":-75.228235170000005,\"POINT_Y\":39.944052130000003,\"HOUR\":null,\"ORIG_FID\":89428846},\"geometry\":{\"x\":-75.228235167999969,\"y\":39.944052125000042}},{\"attributes\":{\"DC_DIST\":\"25\",\"SECTOR\":\"3\",\"DISPATCH_DATE_TIME\":1426150740000,\"DISPATCH_DATE\":\"2015-03-12\",\"DISPATCH_TIME\":\"08:59:00\",\"DC_KEY\":\"201525018938\",\"LOCATION_BLOCK\":\"3400 BLOCK N ORIANNA ST\",\"UCR_GENERAL\":\"100\",\"OBJECTID\":4888,\"TEXT_GENERAL_CODE\":\"Homicide - Criminal \",\"POINT_X\":-75.135453420000005,\"POINT_Y\":40.003027469999999,\"HOUR\":null,\"ORIG_FID\":89431576},\"geometry\":{\"x\":-75.135453419999976,\"y\":40.003027466000049}}]}"
  
  var crimes: Array<Crimes> = [Crimes]()
  var crimeObject: NSDictionary = NSDictionary()
  var incidentsArray: Array<IncidentMarkers.Incidents> = Array<IncidentMarkers.Incidents>()
  var refreshDate = [NSManagedObject]()
  var updateDate: Bool = false
  var coreData = [IncidentsDB]()
  let managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
  let secondsIn24Hours: Int = 86400
  let notificationKey2: String = "com.muludiang.refreshDataNotificationKey"
  let notificationKey1: String = "com.muludiang.loadMapNotificationKey"
  struct Crimes {
    var code: Int?
    var longitude: Double?
    var latitude: Double?
    var textCode: String?
    var block: String?
    var reportDate = NSDate()
  }
  
  // Crime code
  //100: Homicide - red
  //200: Rape - cyan
  //300: Robbery - blue
  //400: Aggravated Assault - green
  //500: Burglary - gray
  //600: Theft - yellow
  //700: Motor Vechile Theft or Recovery - orange
  
  
  
  override init(){
    super.init()
    //checkRefreshDate()
    NSNotificationCenter.defaultCenter().addObserver(self, selector: "userRequestedDataUpdate", name: notificationKey2, object: nil)
    let fetchRequest = NSFetchRequest(entityName: "IncidentsDB")
    if let fetchedResults = (try? managedObjectContext!.executeFetchRequest(fetchRequest)) as? [IncidentsDB] {
      coreData = fetchedResults
    }
    
    if coreData.count == 0 {
      print("New instance")
    }
    //    var crimeData: NSData = crimeJsonData.dataUsingEncoding(NSUTF8StringEncoding)!
    //    var crimeDataError: NSError?
    //println("Init'ing model")
    //      crimeObject = NSJSONSerialization.JSONObjectWithData(crimeData, options: nil, error: &crimeDataError) as! NSDictionary!
    //  OR UCR_GENERAL='200' OR UCR_GENERAL='300' OR UCR_GENERAL='400' OR UCR_GENERAL='500' OR UCR_GENERAL='600' OR UCR_GENERAL='700'
    //    if let moc = self.managedObjectContext {
    if self.coreData.count == 0 {
      fetchIncidentData(false)
      
      
    } else {
      let lastDate = self.coreData.first?.lastUpdated
      if (Int(NSDate().timeIntervalSinceDate(lastDate!)) >= secondsIn24Hours) || (self.coreData.first?.incidents.count < 1 ){
        print("Need to refresh date: \(lastDate)")
        self.fetchIncidentData(true)
      } else {
        print("No refresh needed: \(lastDate)")
        fetchCurrentIncidentData()
      }
    }
    //    }
  }
  
  func fetchCurrentIncidentData() {
    print("Total crimes: \(self.incidentsArray.count)")
    self.crimeObject = coreData.first?.incidents as! NSDictionary
    for (key, value) in self.crimeObject {
      if key as! String == "features" {
        for i in 0...value.count - 1{
          let t: Dictionary = value[i].objectForKey("attributes") as! Dictionary<String,AnyObject>
          var marker: IncidentMarkers.Incidents = IncidentMarkers.Incidents()
          marker.code = t["UCR_GENERAL"]?.integerValue
          marker.latitude = t["POINT_Y"]!.doubleValue
          marker.longitude = t["POINT_X"]!.doubleValue
          marker.textCode = t["TEXT_GENERAL_CODE"] as? String
          marker.block = t["LOCATION_BLOCK"] as? String
          marker.reportDate = NSDate(timeIntervalSince1970: (t["DISPATCH_DATE_TIME"]!.doubleValue / 1000 ))
          self.incidentsArray.append(marker)
        }
      }
    }
    
    let delay = 1 * Double(NSEC_PER_SEC)
    let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
    dispatch_after(time, dispatch_get_main_queue()) {
      NSNotificationCenter.defaultCenter().postNotificationName(self.notificationKey1, object: self)
      print("Total crimes: \(self.incidentsArray.count)")
    }
    
  }
  
  func fetchIncidentData(refreshData: Bool) {
    print("Fetching data...")
    Alamofire.request(.GET, "http://gis.phila.gov/ArcGIS/rest/services/PhilaGov/Police_Incidents_Last30/MapServer/0/query", parameters: ["f":"json","outFields":"*","where":"UCR_GENERAL='100' OR UCR_GENERAL='200' OR UCR_GENERAL='300' OR UCR_GENERAL='400'  OR UCR_GENERAL='500' OR UCR_GENERAL='600' OR UCR_GENERAL='700'"]).responseJSON() {
      request, response, result in
      switch result {
      case .Success(let data):
        self.crimeObject = data as! NSDictionary
        for (key, value) in self.crimeObject {
          if key as! String == "features" {
            for i in 0...value.count - 1{
              let t: Dictionary = value[i].objectForKey("attributes") as! Dictionary<String,AnyObject>
              var marker: IncidentMarkers.Incidents = IncidentMarkers.Incidents()
              marker.code = t["UCR_GENERAL"]?.integerValue
              marker.latitude = t["POINT_Y"]!.doubleValue
              marker.longitude = t["POINT_X"]!.doubleValue
              marker.textCode = t["TEXT_GENERAL_CODE"] as? String
              marker.block = t["LOCATION_BLOCK"] as? String
              marker.reportDate = NSDate(timeIntervalSince1970: (t["DISPATCH_DATE_TIME"]!.doubleValue / 1000 ))
              self.incidentsArray.append(marker)
            }
          }
          
        }
      case .Failure( _, let error):
        print("Request failed with error: \(error)")
      }
      
      if let moc = self.managedObjectContext{
        if !refreshData {
          IncidentsDB.createInManagedObjectContext(moc, newDate: NSDate(), newIncidentsData: self.crimeObject)
        } else {
          IncidentsDB.updateInManagedObjectContext(moc, updatedDate: NSDate(), updatedIncidentsData: self.crimeObject)
        }
        NSNotificationCenter.defaultCenter().postNotificationName(self.notificationKey1, object: self)
      }
    }
    
  }
  
  func getCrimes(codes: Array<Int>) -> Array<Crimes> {
    //        println("Received codes: \(codes)")
    crimes = [Crimes]()
    for code in codes {
      //            println("Getting crimes from code: \(code)")
      findCrimebyCode(code)
    }
    return crimes
  }
  
  private func findCrimebyCode(crimeCode: Int){
    //        println("Looking for crimes with crime code: \(crimeCode)")
    var code: Int?
    var lat: Double?
    var long: Double?
    var block: String?
    var text: String?
    var epoch: Double?
    
    for (myKey, myValue) in crimeObject {
      if myKey as! NSString == "features" {
        myValue.count
        for i in 0...myValue.count-1 {
          let t: NSDictionary = myValue[i].objectForKey("attributes") as! NSDictionary
          if t["UCR_GENERAL"]?.integerValue == crimeCode {
            
            code = t["UCR_GENERAL"]?.integerValue
            lat = t["POINT_Y"]?.doubleValue
            long = t["POINT_X"]?.doubleValue
            text = t["TEXT_GENERAL_CODE"] as? String
            block = t["LOCATION_BLOCK"] as? String
            epoch = t["DISPATCH_DATE_TIME"]?.doubleValue
            crimes.append(Crimes(code: code, longitude: long, latitude: lat, textCode: text, block: block, reportDate: NSDate(timeIntervalSince1970: epoch!/1000)))
          }
        }
      }
    }
  }
  
  func checkRefreshDate() {
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    let managedContext = appDelegate.managedObjectContext!
    let fetchRequest = NSFetchRequest(entityName: "LastRefresh")
    let fetchedResults = try! managedContext.executeFetchRequest(fetchRequest) as! [NSManagedObject]
    //    if let results = fetchedResults {
    
    if fetchedResults.count < 1 {
      print("This is a new instance")
    } else {
      let date: NSDate = fetchedResults.first!.valueForKey("date") as! NSDate
      updateDate = true
      print("Last updated: \(date)")
    }
    //    }
    //    else {
    //      print("Error detected: \(error)")
    //    }
  }
  
  func saveRefreshDate() {
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    let managedContext = appDelegate.managedObjectContext!
    let entity = NSEntityDescription.entityForName("LastRefresh", inManagedObjectContext: managedContext)
    let data = NSManagedObject(entity: entity!, insertIntoManagedObjectContext: managedContext)
    data.setValue(NSDate(), forKey: "date")
    data.setValue(crimeObject, forKey: "incidents")
    //    var error: NSError?
    do {
      try managedContext.save()
      print("New date saved")
    } catch {
      // error = error1
      print("Error saving new date")
    }
    print(data.valueForKey("incidents"))
  }
  
  func updateRefreshDate() {
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    let managedContext = appDelegate.managedObjectContext!
    let fetchRequest = NSFetchRequest(entityName: "LastRefresh")
    let fetchedResults = try! managedContext.executeFetchRequest(fetchRequest)
    fetchedResults.first?.setValue(NSDate(), forKey: "date")
    fetchedResults.first?.setValue(crimeObject, forKey: "incidents")
    do {
      try managedContext.save()
      print("Date updated")
    } catch {
      print("Error updating new date")
    }
  }
}