//
//  IncidentsDB.swift
//  WildSide Philadelphia
//
//  Created by Versatile Systems, Inc on 5/25/15.
//  Copyright (c) 2015 Muludiang IT Services. All rights reserved.
//

import Foundation
import CoreData

class IncidentsDB: NSManagedObject {
  
  @NSManaged var lastUpdated: NSDate
  @NSManaged var incidents: AnyObject
  
  class func createInManagedObjectContext(moc: NSManagedObjectContext, newDate: NSDate, newIncidentsData: NSDictionary) -> IncidentsDB {
    let newData = NSEntityDescription.insertNewObjectForEntityForName("IncidentsDB", inManagedObjectContext: moc) as! IncidentsDB
    newData.lastUpdated = newDate
    newData.incidents = newIncidentsData
    do {
      try moc.save()
      print("New data saved")
    } catch let error as NSError {
      print("Error saving new data: \(error)")
    }
    return newData
  }
  
  class func updateInManagedObjectContext(moc: NSManagedObjectContext, updatedDate: NSDate, updatedIncidentsData: NSDictionary) {
    let fetchRequest = NSFetchRequest(entityName: "IncidentsDB")
    let fetchedResults = try! moc.executeFetchRequest(fetchRequest) as! [NSManagedObject]
    fetchedResults.first?.setValue(updatedDate, forKey: "lastUpdated")
    fetchedResults.first?.setValue(updatedIncidentsData, forKey: "incidents")
    do {
      try moc.save()
    } catch let error as NSError {
      print("Error saving new data: \(error)")
    }
  }
  
}
