//
//  QuadTreeNode.swift
//  WildSide Philadelphia
//
//  Created by Versatile Systems, Inc on 4/20/15.
//  Copyright (c) 2015 Muludiang IT Services. All rights reserved.
//

import UIKit
import MapKit
import GoogleMaps

class QuadTreeNode: NSObject {
  
  let kNodeCapacity: Int = 8
  var name: String = "root node"
  var northWest: QuadTreeNode?
  var northEast: QuadTreeNode?
  var southEast: QuadTreeNode?
  var southWest: QuadTreeNode?
  var bounds: GMSCoordinateBounds?
  var incidentMarkers: IncidentMarkers = IncidentMarkers()
  
  override var description: String {
    return "\(name)"
  }
  
  
  
  override init() {
  }
  
  init(visibleRegion: GMSVisibleRegion) {
    self.bounds = GMSCoordinateBounds(region: visibleRegion)
  }
  
  func isLeaf() -> Bool {
    return northEast != nil ? false : true
  }
  
  func splitNode(node: QuadTreeNode) {
    
    let bounds: GMSCoordinateBounds = node.bounds!

    northWest = QuadTreeNode()
    northWest?.name = "\(node.name) -> northWest"
//    println("Created: \(northWest?.name)")
    
    northEast = QuadTreeNode()
    northEast?.name = "\(node.name) -> northEast"
//    println("Created: \(northEast?.name)")
    
    southWest = QuadTreeNode()
    southWest?.name = "\(node.name) -> southWest"
//    println("Created: \(southWest?.name)")
    
    southEast = QuadTreeNode()
    southEast?.name = "\(node.name) -> southEast"

    let midY: Double = (bounds.southWest.latitude + bounds.northEast.latitude) / 2
    let midX: Double = (bounds.northEast.longitude + bounds.northEast.longitude) / 2
    
    northWest?.bounds = GMSCoordinateBounds(coordinate: CLLocationCoordinate2DMake(midY, bounds.southWest.longitude), coordinate: CLLocationCoordinate2DMake(bounds.northEast.latitude, midX))
    northEast?.bounds = GMSCoordinateBounds(coordinate: CLLocationCoordinate2DMake(midY, midX), coordinate: CLLocationCoordinate2DMake(bounds.northEast.latitude, bounds.northEast.longitude))
    southWest?.bounds = GMSCoordinateBounds(coordinate: CLLocationCoordinate2DMake(bounds.southWest.latitude, bounds.southWest.longitude), coordinate: CLLocationCoordinate2DMake(midY, midX))
    southEast?.bounds = GMSCoordinateBounds(coordinate: CLLocationCoordinate2DMake(bounds.southWest.latitude, midX), coordinate: CLLocationCoordinate2DMake(midY, bounds.northEast.longitude))
  }
  
  
  func clusteredMarkers(bounds: GMSVisibleRegion) -> IncidentMarkers {
    let clusteredMarkers: IncidentMarkers = IncidentMarkers()
    
    if incidentMarkers.markers.count == 0 {
      return clusteredMarkers
    }
    
    for i in 0..<incidentMarkers.markers.count {
      if incidentMarkers.markers[i].latitude >= bounds.nearLeft.latitude && incidentMarkers.markers[i].longitude >= bounds.nearLeft.longitude && incidentMarkers.markers[i].latitude <= bounds.farRight.latitude && incidentMarkers.markers[i].longitude <= bounds.farRight.longitude {
        clusteredMarkers.markers.append(incidentMarkers.markers[i])
      }
    }
   return clusteredMarkers
  }

}
