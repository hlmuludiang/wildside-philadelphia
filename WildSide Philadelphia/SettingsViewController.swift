//
//  SettingsViewController.swift
//  WildSide Philadelphia
//
//  Created by Versatile Systems, Inc on 5/23/15.
//  Copyright (c) 2015 Muludiang IT Services. All rights reserved.
//

import UIKit

protocol SettingsViewControllerDelegate{
  func returnToMapView(controller: SettingsViewController, incidents: Array<Int>)
  func dismissedSettingViewController(controller: SettingsViewController)
}

class SettingsViewController: UIViewController {
  
  @IBOutlet weak var code100: UIButton!
  @IBOutlet weak var code200: UIButton!
  @IBOutlet weak var code300: UIButton!
  @IBOutlet weak var code400: UIButton!
  @IBOutlet weak var code500: UIButton!
  @IBOutlet weak var code600: UIButton!
  @IBOutlet weak var code700: UIButton!
  @IBOutlet weak var cancelButton: UIBarButtonItem!
  @IBOutlet weak var refreshMapSwitch: UISwitch!
  @IBOutlet weak var exitSettingsButton: UIButton!
  var delegate: SettingsViewControllerDelegate! = nil
  var selectedIncidents: Array<Int> = Array<Int>()
  var userRequestedRefresh: Bool = false
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    let buttonImage = UIImage(named: "BlueButton")!.resizableImageWithCapInsets(UIEdgeInsetsMake(18, 18, 18, 18))
    let buttonImageHighlighted = UIImage(named: "WhiteButtonHighlighted")!.resizableImageWithCapInsets(UIEdgeInsetsMake(18, 18, 18,18))
    for button in [100,200,300,400,500,600,700] {
      let tmpButton = self.view.viewWithTag(button) as! UIButton
      tmpButton.setBackgroundImage(buttonImage, forState: UIControlState.Normal)
      tmpButton.setBackgroundImage(buttonImageHighlighted, forState: UIControlState.Selected)
      tmpButton.showsTouchWhenHighlighted = true
    }
    
    for incident in selectedIncidents {
      let tmpButton = self.view.viewWithTag(incident) as! UIButton
      tmpButton.selected = true
    }
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  @IBAction func dismissSettingVC(sender: UIBarButtonItem) {
    //    let incidentsVC: MKViewController = MKViewController()
    //    incidentsVC.adView.startAutomaticallyRefreshingContents()
    //        dismissViewControllerAnimated(true, completion: nil)
//    delegate.returnToMapView(self, showAd: true, incidents: selectedIncidents, refreshData: false)
    delegate.dismissedSettingViewController(self)
  }
  
  @IBAction func mapRefreshRequested(sender: UISwitch) {
    if sender.on {
      userRequestedRefresh = true
    } else {
      userRequestedRefresh = false
    }
  }
  
  @IBAction func updateIncidentsSelected(sender: UIButton) {
    if  sender.selected {
      sender.selected = false
      selectedIncidents = selectedIncidents.filter({$0 != sender.tag})
    } else {
      sender.selected = true
      selectedIncidents
        .append(sender.tag)
    }
  }
  
  
  @IBAction func updateMapFromSettings(sender: UIButton) {
    print("Selected codes: \(selectedIncidents)")
    delegate.returnToMapView(self, incidents: selectedIncidents)
  }
  /*
  // MARK: - Navigation
  
  // In a storyboard-based application, you will often want to do a little preparation before navigation
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
  // Get the new view controller using segue.destinationViewController.
  // Pass the selected object to the new view controller.
  }
  */
  
}
