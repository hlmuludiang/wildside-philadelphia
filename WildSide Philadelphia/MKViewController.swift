//
//  MKViewController.swift
//  WildSide Philadelphia
//
//  Created by Versatile Systems, Inc on 4/17/15.
//  Copyright (c) 2015 Muludiang IT Services. All rights reserved.
//

// Swift blocks: https://thatthinginswift.com/

import UIKit
import MoPub
import QuartzCore
import GoogleMaps

class MKViewController: UIViewController, CLLocationManagerDelegate, GMSMapViewDelegate, MPAdViewDelegate, SettingsViewControllerDelegate
{
  // TODO: Replace this test id with your personal ad unit id
  var adView: MPAdView = MPAdView(adUnitId: "0fd404de447942edb7610228cb412614", size: MOPUB_BANNER_SIZE)
  @IBOutlet weak var incidentmap: GMSMapView!
  @IBOutlet weak var loadMarkersIndicator: UIActivityIndicatorView!
  @IBOutlet weak var accessSettingsButton: UIButton!
  @IBOutlet weak var resetMapButton: UIButton!
  
  @IBOutlet weak var incidentMapTopConstraint: NSLayoutConstraint!
  @IBOutlet weak var resetMapButtonVerticalConstraint: NSLayoutConstraint!
  @IBOutlet weak var incidentMapBottomConstraint: NSLayoutConstraint!
  
  let markerIcon: UIImage = UIImage(named: "google_marker")!
  let clusterIcon: UIImage = UIImage(named: "cluster_marker")!
  
  let locationManager: CLLocationManager = CLLocationManager()
  
  var mapViewMoved: Bool = false
  let notificationKey1: String = "com.muludiang.loadMapNotificationKey"
  let notificationKey2: String = "com.muludiang.refreshDataNotificationKey"
  var incidents: WildSideModel = WildSideModel()
  let tree: QuadTreeManager = QuadTreeManager()
  var myBounds: GMSCoordinateBounds = GMSCoordinateBounds()
  //  var totalVisibleRegion: GMSVisibleRegion = GMSVisibleRegion()
  var mapMovedByUser: Bool = true
  var markerTappedByUser: Bool = false
  var infoWindowTappedByUser: Bool = false
  var resetMapRequested: Bool = false
  var markersAddedToMap: Array<GMSMarker> = Array<GMSMarker>()
  var doneLoading: Bool = false
  var selectedIncidents: Array<Int> = [100,200,300,400,500,600,700]
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    print("Total incidents: \(incidents.incidentsArray.count)")
    NSNotificationCenter.defaultCenter().addObserver(self, selector: "doneLoadingMarkers", name: notificationKey1, object: nil)
    
    incidentmap.delegate = self
    incidentmap.settings.rotateGestures = false
    incidentmap.indoorEnabled = false
    locationManager.delegate = self
    locationManager.requestWhenInUseAuthorization()
    loadMarkersIndicator.startAnimating()
    //let allMarkers: Array<GMSMarker> = tree.getAllMarkers()
    
    adView.delegate = self
    adView.frame = CGRectMake(0, view.frame.size.height - MOPUB_BANNER_SIZE.height,
      MOPUB_BANNER_SIZE.width, MOPUB_BANNER_SIZE.height)
    view.addSubview(adView)
    adView.loadAd()
    //incidentmap = GMSMapView(frame: CGRectMake(incidentmap.frame.origin.x, incidentmap.frame.origin.y, incidentmap.f)
    
  }
  
  func viewControllerForPresentingModalView() -> UIViewController {
    return self
  }
  
//  func renderImageWithColor(color: UIColor, size: CGSize) -> UIImage {
//    let rect: CGRect = CGRectMake(0, 0, size.width, size.height)
//    UIGraphicsBeginImageContext(rect.size)
//    let context: CGContextRef = UIGraphicsGetCurrentContext()
//    
//    CGContextSetFillColorWithColor(context, color.CGColor)
//    CGContextFillRect(context, rect)
//    
//    let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()
//    UIGraphicsEndImageContext()
//    
//    return image
//  }
  
  
  func doneLoadingMarkers() {
    print("Done loading markers: \(incidents.incidentsArray.count)")
    for marker in incidents.incidentsArray {
      myBounds = myBounds.includingCoordinate(CLLocationCoordinate2DMake(marker.latitude!, marker.longitude!))
    }
    
    tree.setBounds(myBounds)
    tree.addMarkers(incidents.incidentsArray)
    incidentmap.animateWithCameraUpdate(GMSCameraUpdate.fitBounds(myBounds))
    doneLoading = true
  }
  
  
  override func viewDidAppear(animated: Bool) {
    super.viewDidAppear(false)
    
  }
  
  func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
    if status == .AuthorizedWhenInUse{
      locationManager.startUpdatingLocation()
      if  locationManager.location != nil {
        //        if bounds.containsCoordinate(locationManager.location.coordinate) {
        incidentmap.myLocationEnabled = true
        incidentmap.settings.myLocationButton = true
        incidentmap.settings.compassButton = true
        //        }
      } else {
        incidentmap.myLocationEnabled = false
        incidentmap.settings.myLocationButton = false
        incidentmap.settings.compassButton = false
      }
    }
  }
  
  func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
    print("Error updating location: \(error)")
  }
  
  func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
    if let _ = locations.last as CLLocation! {
      incidentmap.myLocationEnabled = true
      incidentmap.settings.myLocationButton = true
      incidentmap.settings.compassButton = true
    }
  }
  
  func mapView(mapView: GMSMapView!, willMove gesture: Bool) {
    loadMarkersIndicator.startAnimating()
    if !accessSettingsButton.hidden {
      accessSettingsButton.hidden = true
    }
    if gesture {
      mapView.clear()
      markerTappedByUser = false
      mapMovedByUser = true
      resetMapButton.hidden = false
    }
  }
  
  func mapView(mapView: GMSMapView!, didTapMarker marker: GMSMarker!) -> Bool {
    markerTappedByUser = true
    return false
  }
  
  func mapView(mapView: GMSMapView!, didTapAtCoordinate coordinate: CLLocationCoordinate2D) {
    if !accessSettingsButton.hidden {
      accessSettingsButton.hidden = true
      stopViewShine(accessSettingsButton)
    } else {
            accessSettingsButton.hidden = false
      makeViewShine(accessSettingsButton)
    }
  }
  
  func mapView(mapView: GMSMapView!, idleAtCameraPosition position: GMSCameraPosition!) {
    if !markerTappedByUser || resetMapRequested {
      if markersAddedToMap.count > 0 {
        for marker in markersAddedToMap {
          marker.map = nil
        }
        markersAddedToMap.removeAll(keepCapacity: false)
      }
      incidentmap.clear()
      print("Getting incidents: \(selectedIncidents)")
      let foundIncidents = tree.getAllMarkersForBounds(GMSCoordinateBounds(region: mapView.projection.visibleRegion()), withMap: mapView, withIncidents: selectedIncidents)
      for incident in foundIncidents {
        let incidentMarker: GMSMarker? = GMSMarker()
        incidentMarker!.position = CLLocationCoordinate2DMake(incident.latitude!, incident.longitude!)
        incidentMarker!.flat = false
        if incident.code == 99999 {
          incidentMarker!.icon = clusterIcon
          incidentMarker!.userData = incident.clusterBounds
          incidentMarker!.snippet = "Tap this window to view the incidents"
        } else {
          incidentMarker!.icon = markerIcon
          let dateFormat: NSDateFormatter = NSDateFormatter()
          dateFormat.dateFormat = "EEE, MMM d YYYY: h:mm a"
          incidentMarker!.snippet = "Location: \(incident.block! as String) \n Date Occurred: \(dateFormat.stringFromDate(incident.reportDate))"
        }
        incidentMarker!.title = incident.textCode
        incidentMarker!.map = mapView
        markersAddedToMap.append(incidentMarker!)
        resetMapRequested = false
      }
    }
    
    if infoWindowTappedByUser {
      //      println("Marker tapped!")
      
      if markersAddedToMap.count > 0 {
        for marker in markersAddedToMap {
          marker.map = nil
        }
        markersAddedToMap.removeAll(keepCapacity: false)
      }
      infoWindowTappedByUser = false
      if mapView.selectedMarker.userData != nil {
        
        let foundIncidents = tree.getAllMarkersForBounds(mapView.selectedMarker.userData as! GMSCoordinateBounds, withMap: mapView, withIncidents: selectedIncidents)
        incidentmap.selectedMarker = nil
        incidentmap.clear()
        
        for incident in foundIncidents {
          let incidentMarker: GMSMarker? = GMSMarker()
          incidentMarker!.position = CLLocationCoordinate2DMake(incident.latitude!, incident.longitude!)
          incidentMarker!.flat = false
          if incident.code == 99999 {
            incidentMarker!.icon = clusterIcon
            incidentMarker!.userData = incident.clusterBounds
            incidentMarker!.snippet = "Tap this window to view the incidents"
          } else {
            incidentMarker!.icon = markerIcon
            let dateFormat: NSDateFormatter = NSDateFormatter()
            dateFormat.dateFormat = "EEE, MMM d YYYY: h:mm a"
            incidentMarker!.snippet = "Location: \(incident.block! as String) \n Date Occurred: \(dateFormat.stringFromDate(incident.reportDate))"
          }
          incidentMarker!.title = incident.textCode
          incidentMarker!.map = mapView
          markersAddedToMap.append(incidentMarker!)
        }
      }
    }
    //      println("Displaying \(foundIncidents.count)")
    incidentmap.settings.setAllGesturesEnabled(true)
    incidentmap.settings.rotateGestures = false
    loadMarkersIndicator.stopAnimating()
  }
  
  func didTapMyLocationButtonForMapView(mapView: GMSMapView!) -> Bool {
    loadMarkersIndicator.startAnimating()
    incidentmap.camera = GMSCameraPosition(target: locationManager.location!.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
    mapView.selectedMarker = nil
    resetMapButton.hidden = false
    return false
  }
  
  func mapView(mapView: GMSMapView!, didTapInfoWindowOfMarker marker: GMSMarker!) {
    if marker.userData != nil {
      loadMarkersIndicator.startAnimating()
      incidentmap.settings.setAllGesturesEnabled(false)
      infoWindowTappedByUser = true
      mapView.animateWithCameraUpdate(GMSCameraUpdate.fitBounds(marker.userData as! GMSCoordinateBounds))
      resetMapButton.hidden = false
    }
  }
  
  @IBAction func resetIncidentMap(sender: UIButton) {
    loadMarkersIndicator.startAnimating()
    incidentmap.animateWithCameraUpdate(GMSCameraUpdate.fitBounds(myBounds))
    resetMapRequested = true
    resetMapButton.hidden = true
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(false)
  }
  
  override func willRotateToInterfaceOrientation(toInterfaceOrientation: UIInterfaceOrientation, duration: NSTimeInterval) {
    adView.rotateToOrientation(toInterfaceOrientation)
  }
  
  override func didRotateFromInterfaceOrientation(fromInterfaceOrientation: UIInterfaceOrientation) {
    let size: CGSize = adView.adContentViewSize()
    let centeredX: CGFloat = (view.bounds.size.width - size.width) / 2
    let bottomAlignedY: CGFloat = view.bounds.size.height - size.height
    adView.frame = CGRectMake(centeredX, bottomAlignedY, size.width, size.height)
  }
  
  func adViewDidLoadAd(view: MPAdView!) {
    if !mapViewMoved {
      self.view.layoutIfNeeded()
      incidentMapBottomConstraint.constant = view.frame.height
      resetMapButtonVerticalConstraint.constant += view.frame.height
      incidentmap.setNeedsUpdateConstraints()
      UIView.animateWithDuration(1, animations: { () -> Void in
        self.view.layoutIfNeeded()
      })
      mapViewMoved = true
    }
  }
  
  func returnToMapView(controller: SettingsViewController, incidents: Array<Int>) {
    controller.dismissViewControllerAnimated(true, completion: nil)
    print("We are back...")
      adView.startAutomaticallyRefreshingContents()
    selectedIncidents = incidents
            resetMapRequested = true
    incidentmap.animateWithCameraUpdate(GMSCameraUpdate.fitBounds(myBounds))
  }
  
  func dismissedSettingViewController(controller: SettingsViewController) {
          print("We are back - VC dismissed...")
    adView.startAutomaticallyRefreshingContents()
    incidentmap.animateWithCameraUpdate(GMSCameraUpdate.fitBounds(myBounds))
        controller.dismissViewControllerAnimated(true, completion: nil)
  }
  
  // MARK: - Navigation
  
  // In a storyboard-based application, you will often want to do a little preparation before navigation
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    print("We're outta here...")
    if segue.identifier == "settingsVCSegue" {
      accessSettingsButton.hidden = true
      stopViewShine(accessSettingsButton)
      incidentmap.animateToZoom(incidentmap.camera.zoom * 0.95)
//      if mapViewMoved {
//        self.view.layoutIfNeeded()
//        incidentMapBottomConstraint.constant = 0
//        resetMapButtonVerticalConstraint.constant -= view.frame.height
//        incidentmap.setNeedsUpdateConstraints()
//        UIView.animateWithDuration(0, animations: { () -> Void in
//          self.view.layoutIfNeeded()
//        })
//        mapViewMoved = false
//      }
      adView.stopAutomaticallyRefreshingContents()
      let settingsVC: SettingsViewController = segue.destinationViewController as! SettingsViewController
      settingsVC.selectedIncidents = selectedIncidents
      settingsVC.delegate = self
    }
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
  }
  
  
  private func makeViewShine(view: UIView) {
    view.layer.shadowColor = UIColor.blueColor().CGColor
    view.layer.shadowRadius = 10.0
    view.layer.shadowOpacity = 1.0
    view.layer.shadowOffset = CGSizeZero
    
    
    UIView.animateWithDuration(0.7, delay: 0, options: [UIViewAnimationOptions.Autoreverse, UIViewAnimationOptions.CurveEaseInOut, UIViewAnimationOptions.Repeat, UIViewAnimationOptions.AllowUserInteraction], animations: {
//      () -> Void in
      UIView.setAnimationRepeatCount(15)
      view.transform = CGAffineTransformMakeScale(1.2, 1.2) }, completion: nil)
//    }) { (Bool finished) -> Void in
//      self.makeViewShine(view)
//    }
  }
  
  private func stopViewShine(view: UIView) {
    view.layer.shadowRadius = 0
    view.transform = CGAffineTransformMakeScale(1, 1)
    view.layer.removeAllAnimations()
  }
  
}
