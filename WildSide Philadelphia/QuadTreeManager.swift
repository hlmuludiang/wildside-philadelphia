//
//  QuadTreeManager.swift
//  WildSide Philadelphia
//
//  Created by Versatile Systems, Inc on 4/20/15.
//  Copyright (c) 2015 Muludiang IT Services. All rights reserved.
//

import UIKit
import GoogleMaps

class QuadTreeManager: NSObject {
  
  var quadTree: QuadTree = QuadTree()
  let cellSizeForCoordinator: Double = 1.5
  
  override init(){
    
  }
  
  //  init(markers: Array<GMSMarker>, forBounds: GMSCoordinateBounds){
  //    super.init()
  //    addMarkers(markers)
  //  }
  
  func addMarkers(markers: Array<IncidentMarkers.Incidents>) {
    for marker in markers {
      quadTree.insertMarker(marker)
    }
  }
  
  func getClusterMarkers(map: GMSMapView) -> Array<IncidentMarkers.Incidents> {
    return quadTree.getAllMarkersForBounds(map.projection.visibleRegion())
  }
  
  func getAllMarkersForBounds(map: GMSMapView, withIncidents: Array<Int>) -> Array<IncidentMarkers.Incidents> {
    return clusteredMarkersForBounds(map.projection.visibleRegion(), withMap: map, forSelectedIncidents: withIncidents)
  }
  
  func getAllMarkersForBounds(bounds: GMSCoordinateBounds, withMap: GMSMapView, withIncidents: Array<Int>) -> Array<IncidentMarkers.Incidents> {
    var vr: GMSVisibleRegion = GMSVisibleRegion()
    vr.nearLeft.latitude = bounds.southWest.latitude
    vr.nearLeft.longitude = bounds.southWest.longitude
    vr.farRight.latitude = bounds.northEast.latitude
    vr.farRight.longitude = bounds.northEast.longitude
    
    return clusteredMarkersForBounds(vr, withMap: withMap, forSelectedIncidents: withIncidents)
  }
  
  
  func getAllMarkers(withIncidents: Array<Int>) -> Array<IncidentMarkers.Incidents> {
    return quadTree.getAllMarkers(withIncidents)
  }
  
  init(visibleRegion: GMSVisibleRegion) {
    quadTree = QuadTree(visibleRegion: visibleRegion)
  }
  
  func setBounds(mapBounds: GMSCoordinateBounds) {
    quadTree.rootNode.bounds = mapBounds
  }
  
  func clusteredMarkersForBounds(bounds: GMSVisibleRegion, withMap: GMSMapView, forSelectedIncidents: Array<Int>) -> Array<IncidentMarkers.Incidents> {
    //    println("Zoom: \(withMap.camera.zoom)")
    
    let tiles: Double = cellSizeForZoomLevel(withMap)
    let tilesPerSide: Double = round(sqrt(tiles) / 4)
    
    var clusteredMarkers: Array<IncidentMarkers.Incidents> = Array<IncidentMarkers.Incidents>()
    
    let latitudeSlice = (bounds.farRight.latitude - bounds.nearLeft.latitude) / tilesPerSide   //(bounds.nearLeft.latitude + bounds.farRight.latitude) / tilesPerSide
    let longititudeSlice = (bounds.farRight.longitude - bounds.nearLeft.longitude) / tilesPerSide
    
    for x in 0..<Int(tilesPerSide) {
      for y in 0..<Int(tilesPerSide) {
        let y0 = bounds.nearLeft.latitude + (latitudeSlice * Double(y))
        let yf = bounds.nearLeft.latitude + (latitudeSlice * (Double(y) + 1))
        let x0 = bounds.nearLeft.longitude + (longititudeSlice * Double(x))
        let xf = bounds.nearLeft.longitude + (longititudeSlice * (Double(x) + 1))
        
        let searchSectorBounds: GMSCoordinateBounds = GMSCoordinateBounds(coordinate: CLLocationCoordinate2DMake(y0, x0), coordinate: CLLocationCoordinate2DMake(yf, xf))
        
        let someArray = quadTree.enumerateMarkersInBounds(searchSectorBounds, forIncidents: forSelectedIncidents)
        
        if someArray.count == 1 {
          clusteredMarkers.append(someArray[0])
        }
        if someArray.count > 1 {
          let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
          var tempLabel: IncidentMarkers.Incidents = IncidentMarkers.Incidents()
          var totalLatitude: Double = 0
          var totalLongitude: Double = 0
          dispatch_async(dispatch_get_global_queue(priority, 0)) {
            for marker in someArray {
              totalLatitude += marker.latitude!
              totalLongitude += marker.longitude!
              tempLabel.clusterBounds = tempLabel.clusterBounds.includingCoordinate(CLLocationCoordinate2DMake(marker.latitude!, marker.longitude!))
            }
            tempLabel.latitude = totalLatitude / Double(someArray.count)
            tempLabel.longitude = totalLongitude / Double(someArray.count)
            tempLabel.textCode = "\(someArray.count) incidents reported here"
            tempLabel.code = 99999
            tempLabel.block = ""
            tempLabel.reportDate = NSDate()
            clusteredMarkers.append(tempLabel)
          }
        }
      }
    }
    return clusteredMarkers
  }
  
  func cellSizeForZoomLevel(map: GMSMapView) -> Double{
    //    println("Zoom: \(map.camera.zoom) | Tiles: \(pow(2,round(map.camera.zoom)))")
    switch round(map.camera.zoom) {
    case 0...14:
      return Double(pow(2,round(map.camera.zoom)))
    default:
      return pow(2,15)
    }
    //return Double(pow(2,round(map.camera.zoom)))
  }
  
}
  