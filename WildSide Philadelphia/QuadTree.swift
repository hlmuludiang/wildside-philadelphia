//
//  QuadTree.swift
//  WildSide Philadelphia
//
//  Created by Versatile Systems, Inc on 4/20/15.
//  Copyright (c) 2015 Muludiang IT Services. All rights reserved.
//

import UIKit
import GoogleMaps

class QuadTree: NSObject {
  
  var rootNode: QuadTreeNode = QuadTreeNode()
  
  override init(){
  }
  
  init(visibleRegion: GMSVisibleRegion) {
    rootNode = QuadTreeNode(visibleRegion: visibleRegion)
  }
  
  func clearMarkers() {
    rootNode = QuadTreeNode()
  }
  
  func insertMarker(marker: IncidentMarkers.Incidents) -> Bool {
    return insertMarker(marker, node: rootNode)
  }
  
  func insertMarker(marker: IncidentMarkers.Incidents, node: QuadTreeNode) -> Bool {
    
    if !node.bounds!.containsCoordinate(CLLocationCoordinate2DMake(marker.latitude!, marker.longitude!)) {
      return false
    }
    
    // println("Bounds: \(node.bounds.nearLeft.latitude)")
    if node.isLeaf() {
      node.splitNode(node)
    }
    if node.incidentMarkers.markers.count < node.kNodeCapacity {
      //      println("Inserted marker into node: \(node.name)")
      node.incidentMarkers.markers.append(marker)
      return true
    }
    if insertMarker(marker, node: node.northWest!) {
      return true
    }
    if insertMarker(marker, node: node.northEast!) {
      return true
    }
    if insertMarker(marker, node: node.southWest!) {
      return true
    }
    if insertMarker(marker, node: node.southEast!) {
      return true
    }
    return false
  }
  
  func enumerateMarkersInBounds(bounds: GMSCoordinateBounds, forIncidents: Array<Int>) -> Array<IncidentMarkers.Incidents> {
    //return enumerateMarkersInBoundsUsingNode(bounds, node: rootNode)
    var markersArray: Array<IncidentMarkers.Incidents> = Array<IncidentMarkers.Incidents>()
    enumerateMarkersInBoundsUsingNode(bounds, node: rootNode, incidents: forIncidents) { (foundMarkers) -> Void in
      markersArray =  foundMarkers
    }
    //    println("Markers returned: \(markersArray.count)")
    return markersArray
  }
  
  func enumerateMarkersInBoundsUsingNode(bounds: GMSCoordinateBounds, node: QuadTreeNode, incidents: Array<Int>, completionHandler:(foundMarkers: Array<IncidentMarkers.Incidents>) -> Void) {
    //     println("Checking: \(node.description)")
    
    var markers: Array<IncidentMarkers.Incidents> = Array<IncidentMarkers.Incidents>()
    if node.bounds?.intersectsBounds(bounds) == true && node.isLeaf() == false {
      //      completionHandler(foundMarkers: [])
      //      println("I should not see this for out of bounds")
      //    } else {
      
      //      var markers: Array<IncidentMarkers.Incidents> = Array<IncidentMarkers.Incidents>()
      
      for marker in node.incidentMarkers.markers {
        for incident in incidents {
          if bounds.containsCoordinate(CLLocationCoordinate2DMake(marker.latitude!, marker.longitude!)) && marker.code == incident {
            markers.append(marker)
          }
        }
      }
      
      //      if node.isLeaf() {
      //        return
      //
      //      }
      
      self.enumerateMarkersInBoundsUsingNode(bounds, node: node.northWest!, incidents: incidents) { (foundMarkers) -> Void in
        markers = markers + foundMarkers
      }
      self.enumerateMarkersInBoundsUsingNode(bounds, node: node.northEast!, incidents: incidents) { (foundMarkers) -> Void in
        markers = markers + foundMarkers
      }
      self.enumerateMarkersInBoundsUsingNode(bounds, node: node.southWest!, incidents: incidents) { (foundMarkers) -> Void in
        markers = markers + foundMarkers
      }
      self.enumerateMarkersInBoundsUsingNode(bounds, node: node.southEast!, incidents: incidents) { (foundMarkers) -> Void in
        markers = markers + foundMarkers
      }
      completionHandler(foundMarkers: markers)
    }
  }
  
  func getAllMarkers(selectedIncidents: Array<Int>) -> Array<IncidentMarkers.Incidents> {
    return getMarkers(rootNode, withIncidents: selectedIncidents)
  }
  
  func getMarkers(node: QuadTreeNode, withIncidents: Array<Int>) -> Array<IncidentMarkers.Incidents> {
    var theMarkers: Array<IncidentMarkers.Incidents> = Array<IncidentMarkers.Incidents>()
    //    println("\(node.markers.count) found in \(node.name)")
    for marker in node.incidentMarkers.markers {
      //            println("Found markers: \(theMarkers.count)")
      for incident in withIncidents {
        if marker.code == incident {
          theMarkers.append(marker)
        }
      }
      
    }
    if node.isLeaf() {
      return theMarkers
    }
    if node.northWest != nil && node.northWest?.incidentMarkers.markers.count > 0 {
      theMarkers = theMarkers + getMarkers(node.northWest!, withIncidents: withIncidents)
    }
    if node.northEast != nil && node.northEast?.incidentMarkers.markers.count > 0 {
      theMarkers = theMarkers +  getMarkers(node.northEast!, withIncidents: withIncidents)
      
    }
    if node.southWest != nil && node.southWest?.incidentMarkers.markers.count > 0 {
      theMarkers = theMarkers + getMarkers(node.southWest!, withIncidents: withIncidents)
    }
    if node.southEast != nil && node.southEast?.incidentMarkers.markers.count > 0 {
      theMarkers = theMarkers + getMarkers(node.southEast!, withIncidents: withIncidents)
    }
    return theMarkers
  }
  
  func getAllMarkersForBounds(bounds: GMSVisibleRegion) -> Array<IncidentMarkers.Incidents> {
    return getAllMarkersForBounds(rootNode, forBounds: bounds)
  }
  
  func getAllMarkersForBounds(node: QuadTreeNode, forBounds: GMSVisibleRegion) -> Array<IncidentMarkers.Incidents> {
    var foundMarkers: Array<IncidentMarkers.Incidents> = Array<IncidentMarkers.Incidents>()
    //    println("NL: \(forBounds.nearLeft.latitude) | FR: \(forBounds.farRight.latitude)")
    //    println("NLg: \(forBounds.nearLeft.longitude) | FLg: \(forBounds.farRight.longitude)")
    for marker in node.incidentMarkers.markers {
      if forBounds.nearLeft.latitude <= marker.latitude &&
        forBounds.farRight.latitude >= marker.latitude &&
        forBounds.nearLeft.longitude <= marker.longitude &&
        forBounds.farRight.longitude >= marker.longitude {
          foundMarkers.append(marker)
      }
    }
    if node.northWest != nil && node.northWest?.incidentMarkers.markers.count > 0 {
      foundMarkers = foundMarkers + getAllMarkersForBounds(node.northWest!, forBounds: forBounds)
    }
    if node.northEast != nil && node.northEast?.incidentMarkers.markers.count > 0 {
      foundMarkers = foundMarkers + getAllMarkersForBounds(node.northEast!, forBounds: forBounds)
    }
    if node.southWest != nil && node.southWest?.incidentMarkers.markers.count > 0 {
      foundMarkers = foundMarkers + getAllMarkersForBounds(node.southWest!, forBounds: forBounds)
    }
    if node.southEast != nil && node.southEast?.incidentMarkers.markers.count > 0 {
      foundMarkers = foundMarkers + getAllMarkersForBounds(node.southEast!, forBounds: forBounds)
    }
    return foundMarkers
  }
}

