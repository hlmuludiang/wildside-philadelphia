//
//  IncidentMarkers.swift
//  WildSide Philadelphia
//
//  Created by Versatile Systems, Inc on 4/30/15.
//  Copyright (c) 2015 Muludiang IT Services. All rights reserved.
//

import UIKit
import GoogleMaps

class IncidentMarkers: NSObject {
  
  var markers: Array<Incidents> = [Incidents]()
  
  var marker: Incidents = Incidents()
  
  struct Incidents {
    var code: Int?
    var longitude: Double?
    var latitude: Double?
    var textCode: String?
    var block: String?
    var reportDate: NSDate = NSDate()
    var clusterBounds: GMSCoordinateBounds = GMSCoordinateBounds()
  }
}
