//
//  MarshallOperator.swift
//  WildSide Philadelphia
//
//  Created by Versatile Systems, Inc on 5/6/15.
//  Copyright (c) 2015 Muludiang IT Services. All rights reserved.
//

import Foundation

infix operator ~> {}

func ~> (
  backgroundClosure: () -> (),
  mainClosure: () -> ()){
    dispatch_async(queue) {
      backgroundClosure()
      dispatch_async(dispatch_get_main_queue(), mainClosure)
    }
}

func ~> <R> (
  backgroundClosure: () -> R,
  mainClosure:       (result: R) -> ())
{
  dispatch_async(queue) {
    let result = backgroundClosure()
    dispatch_async(dispatch_get_main_queue(), {
      mainClosure(result: result)
    })
  }
}

private let queue = dispatch_queue_create("serial-worker", DISPATCH_QUEUE_SERIAL)